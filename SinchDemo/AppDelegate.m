#import "AppDelegate.h"
#import <Sinch/Sinch.h>

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  [self handleLocalNotification:[launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey]];
  [self requestUserNotificationPermission];
  [[NSNotificationCenter defaultCenter] addObserverForName:@"UserDidLoginNotification"
                                                    object:nil
                                                     queue:nil
                                                usingBlock:^(NSNotification *note) {
                                                  [self initSinchClientWithUserId:note.userInfo[@"userId"]];
                                                }];
  return YES;
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
  [self handleLocalNotification:notification];
}

- (void)requestUserNotificationPermission {
  if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]) {
    UIUserNotificationType types = UIUserNotificationTypeAlert | UIUserNotificationTypeSound;
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
  }
}

#pragma mark -

- (void)initSinchClientWithUserId:(NSString *)userId {
  if (!_client) {
    _client = [Sinch clientWithApplicationKey:@"fe0724e6-86b6-472f-ac90-13fbc1227a9e"
                            applicationSecret:@"NACZna+NV0+ElAftmCzIFQ=="
                              environmentHost:@"clientapi.sinch.com"
                                       userId:userId];
    _client.delegate = self;

    [_client setSupportCalling:YES];

    [_client start];
    [_client startListeningOnActiveConnection];
  }
}

- (void)handleLocalNotification:(UILocalNotification *)notification {
  if (notification) {
    id<SINNotificationResult> result = [self.client relayLocalNotification:notification];
    if ([result isCall] && [[result callResult] isTimedOut]) {
      UIAlertView *alert = [[UIAlertView alloc]
              initWithTitle:@"Missed call"
                    message:[NSString stringWithFormat:@"Missed call from %@", [[result callResult] remoteUserId]]
                   delegate:nil
          cancelButtonTitle:nil
          otherButtonTitles:@"OK", nil];
      [alert show];
    }
  }
}

#pragma mark - SINClientDelegate

- (void)clientDidStart:(id<SINClient>)client {
  NSLog(@"Sinch client started successfully (version: %@)", [Sinch version]);
  [[NSNotificationCenter defaultCenter] postNotificationName:@"didRegisterSinch" object:nil userInfo:nil];
}

- (void)clientDidFail:(id<SINClient>)client error:(NSError *)error {
  NSLog(@"Sinch client error: %@", [error localizedDescription]);
  [[NSNotificationCenter defaultCenter] postNotificationName:@"didRegisterSinch" object:nil userInfo:@{@"error":error}];
}

- (void)client:(id<SINClient>)client
    logMessage:(NSString *)message
          area:(NSString *)area
      severity:(SINLogSeverity)severity
     timestamp:(NSDate *)timestamp {
  NSLog(@"%@", message);
}

@end
