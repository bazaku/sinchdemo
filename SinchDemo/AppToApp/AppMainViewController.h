#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface AppMainViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *userIdLabel;
@property (weak, nonatomic) IBOutlet UITextField *destination;
@property (weak, nonatomic) IBOutlet UIButton *callButton;

- (IBAction)call:(id)sender;

@end
