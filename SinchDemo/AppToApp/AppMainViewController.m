#import "AppMainViewController.h"
#import "AppCallViewController.h"

#import <Sinch/Sinch.h>

@interface AppMainViewController () <SINCallClientDelegate>
@end

@implementation AppMainViewController

- (id<SINClient>)client {
  return [(AppDelegate *)[[UIApplication sharedApplication] delegate] client];
}

- (void)awakeFromNib {
  [super awakeFromNib];
  self.client.callClient.delegate = self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  self.userIdLabel.text = self.client.userId;
}

- (IBAction)call:(id)sender {
  if ([self.destination.text length] > 0 && [self.client isStarted]) {
    id<SINCall> call = [self.client.callClient callUserWithId:self.destination.text];
    [self performSegueWithIdentifier:@"callView" sender:call];
  }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  AppCallViewController *callViewController = [segue destinationViewController];
  callViewController.call = sender;
}

#pragma mark - SINCallClientDelegate

- (void)client:(id<SINCallClient>)client didReceiveIncomingCall:(id<SINCall>)call {
  [self performSegueWithIdentifier:@"callView" sender:call];
}

- (SINLocalNotification *)client:(id<SINClient>)client localNotificationForIncomingCall:(id<SINCall>)call {
  SINLocalNotification *notification = [[SINLocalNotification alloc] init];
  notification.alertAction = @"Answer";
  notification.alertBody = [NSString stringWithFormat:@"Incoming call from %@", [call remoteUserId]];
  return notification;
}

@end
