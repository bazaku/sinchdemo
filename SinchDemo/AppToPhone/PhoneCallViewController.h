#import <UIKit/UIKit.h>
#import <Sinch/Sinch.h>

#import "SINUIViewController.h"

@interface PhoneCallViewController : SINUIViewController

@property (weak, nonatomic) IBOutlet UILabel *remoteUsername;
@property (weak, nonatomic) IBOutlet UILabel *callStateLabel;
@property (weak, nonatomic) IBOutlet UIButton *endCallButton;

@property (nonatomic, readwrite, strong) NSTimer *durationTimer;

@property (nonatomic, readwrite, strong) id<SINCall> call;

- (IBAction)hangup:(id)sender;

@end
