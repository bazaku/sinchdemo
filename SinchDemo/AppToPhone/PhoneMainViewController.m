#import "PhoneMainViewController.h"
#import "PhoneCallViewController.h"

#import <Sinch/Sinch.h>

@implementation PhoneMainViewController

- (id<SINClient>)client {
  return [(AppDelegate *)[[UIApplication sharedApplication] delegate] client];
}

- (IBAction)call:(id)sender {
  if ([self.destination.text length] > 0 && [self.client isStarted]) {
    id<SINCall> call = [self.client.callClient callPhoneNumber:self.destination.text];
    [self performSegueWithIdentifier:@"callView" sender:call];
  }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  PhoneCallViewController *callViewController = [segue destinationViewController];
  callViewController.call = sender;
}
- (void)viewDidLoad {
  [super viewDidLoad];
  self.userIdLabel.text = self.client.userId;
}

@end
