#import "LoginViewController.h"
#import "AppDelegate.h"

@import SVProgressHUD;

@interface LoginViewController () <UITextFieldDelegate>
@property (nonatomic, strong) NSString *segueId;
@end

@implementation LoginViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  [self.nameTextField becomeFirstResponder];
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRegister:) name:@"didRegisterSinch" object:nil];
}

- (IBAction)callToApp:(id)sender {
  [self registerName];
  self.segueId = @"app-to-app";
}

- (IBAction)callToPhone:(id)sender {
  [self registerName];
  self.segueId = @"app-to-phone";
}

- (IBAction)videoCall:(id)sender {
  [self registerName];
  self.segueId = @"video-call";
}
   
- (void)didRegister:(NSNotification*)notification {
  [SVProgressHUD dismiss];
  [self performSegueWithIdentifier:self.segueId sender:nil];
}

- (void)registerName {
  if ([self.nameTextField.text length] == 0) {
    return;
  }
  [SVProgressHUD show];
  [[NSNotificationCenter defaultCenter] postNotificationName:@"UserDidLoginNotification"
                                                      object:nil
                                                    userInfo:@{
                                                               @"userId" : self.nameTextField.text
                                                               }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
  [textField resignFirstResponder];
  return YES;
}

@end
